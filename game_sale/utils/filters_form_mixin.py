# -*- coding: utf-8 -*-
from django.forms import MultipleChoiceField
from django.views.generic.edit import FormMixin
import urllib
from urllib.parse import urlencode


class FiltersFormMixin(FormMixin):
    """
    Миксин для ListView, позволяет с помощью форм сделать фильтры для выводимых данных,
    причем составление запросов нужно делать вручную.
    Дополнительно формирует URL с фильтрами, чтобы можно было сохранить страницу с
    определенным набором фильтров

    Пример использования:

    class SearchView(FiltersFormMixin, ListView):
        template_name = 'realty/search.html'
        success_url = '/'
        form_class = SearchForm  # Формы, по которым будем фильтровать недвижимость

        def get_queryset(self):
            # Получаем словать с фильтрами и создаем queryset вручную
            filters = self.get_filters()
            queryset = Realty.objects.filter(is_publish=True)
            if 'district' in filters:
                queryset = queryset.filter(district=int(filters['district']))
            return queryset
    """
    filter_in_get = True  # Использовать ли для фильтров GET-параметры
    #filter_prefix = ''

    def form_invalid(self, form):
        pass

    def form_valid(self, form):
        """
        Сохраняем фильтры из POST запроса в COOKIE, и если нужно, дополнительно формируем URL
        """
        url_filters = {}
        cookie_filters = {}
        for field, value in form.cleaned_data.items():
            if value:
                # Если поле является ссылкой, то берем его id, а не имя
                value = getattr(value, 'id', value)
                if isinstance(value, list):
                    value = ','.join(value)

                url_filters[field] = value
                cookie_filters[field] = value
            else:
                cookie_filters[field] = ''

        if self.filter_in_get and url_filters:
            url_filters = urlencode(url_filters)
            self.success_url += '?' + url_filters

        response = super(FiltersFormMixin, self).form_valid(form)
        for filter, value in cookie_filters.items():
            response.set_cookie(filter, urllib.parse.quote(value))

        return response

    def get_context_data(self, **kwargs):
        form_class = self.get_form_class()
        context = {'form': self.get_form(form_class)}
        context.update(kwargs)
        return super(FiltersFormMixin, self).get_context_data(**context)

    def post(self, request, *args, **kwargs):
        """
        Код из ProcessFormView, запускает валидацию форм-фильтров
        """
        form_class = self.get_form_class()
        form = self.get_form(form_class)
        if form.is_valid():
            return self.form_valid(form)
        else:
            self.form_invalid(form)
            return self.get(self, request, *args, **kwargs)

    def render_to_response(self, context, **kwargs):
        # Сохраним все пришедшие из GET фильтры в COOKIE
        response = super(FiltersFormMixin, self).render_to_response(context, **kwargs)
        if self.filter_in_get:
            for field in self.form_class.base_fields:
                value = self.request.GET.get(field, '')
                if value:
                    response.set_cookie(field, urllib.parse.quote(value))
        return response

    def get_initial(self):
        return self.get_filters()

    def get_filters(self):
        # Вытащим все фильтры из COOKIE, но если есть фильтр в GET, то берем из GET
        filters = {}
        for field, form in self.form_class.base_fields.items():
            value = urllib.parse.unquote(self.request.COOKIES.get(field, ''))
            if field in self.request.GET:
                value = self.request.GET.get(field, '')
            if value:
                if isinstance(form, MultipleChoiceField):
                    value = value.split(',')
                filters[field] = value
        return filters