# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from sales.views import ParseView, StoresView, SalesView, BestView, SearchView, SaleView, SleepView


urlpatterns = patterns('',
    url(r'^sleep/$', SleepView.as_view()),
    url(r'^parse/$', ParseView.as_view()),
    url(r'^parse/(?P<store_name>\w+)/$', ParseView.as_view()),
    url(r'^stores/$', StoresView.as_view()),
    url(r'^best/$', BestView.as_view()),
    url(r'^(?P<store_name>\w+)/search/$', SearchView.as_view()),
    url(r'^(?P<store_name>\w+)/$', SalesView.as_view()),
)