# -*- coding: utf-8 -*-
from django import forms
from parser.parse import STORES

CHOICES_STORE = [(s, STORES[s]['name']) for s in STORES]
#CHOICES_STORE.insert(0, ('', 'магазин...'))


class MainSearchForm(forms.Form):
    # store = forms.ChoiceField(choices=CHOICES_STORE, required=False,
    #                           widget=forms.Select(attrs={'class': 'form-control'}))
    stores = forms.MultipleChoiceField(choices=CHOICES_STORE, required=False,
                                       widget=forms.SelectMultiple())
    price_from = forms.IntegerField(
        required=False,
        widget=forms.NumberInput(attrs={'class': 'form-control', 'style': 'max-width: 120px;', 'placeholder': 'цена от...'})
    )
    price_to = forms.IntegerField(
        required=False,
        widget=forms.NumberInput(attrs={'class': 'form-control', 'style': 'max-width: 120px;', 'placeholder': 'цена до...'})
    )
    name = forms.CharField(
        required=False,
        widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': 'название...'})
    )