from django.db import models
import datetime
from parser.parse import STORES
from django.utils.timezone import utc

CHOICES_STORES = [(s, s) for s in STORES]


class Sale(models.Model):
    store = models.CharField(max_length=100, choices=CHOICES_STORES)
    name = models.CharField(max_length=255)
    name_real = models.CharField(max_length=255, blank=True)

    link = models.CharField(max_length=255, blank=True)
    image = models.CharField(max_length=255, blank=True)

    price_old = models.FloatField(default=0)
    price_now = models.FloatField(default=0)
    discount = models.PositiveIntegerField(default=0)

    is_bundle = models.BooleanField(default=False)
    is_dlc = models.BooleanField(default=False)

    is_active = models.BooleanField(default=False)
    expire = models.DateTimeField(default=lambda: datetime.datetime.now().replace(tzinfo=utc) + datetime.timedelta(days=1))
    updated = models.DateTimeField()
    price_updated = models.DateTimeField()  # Дата последнего обновления цены

    def __str__(self):
        return self.store + ' - ' + self.name


class Store(models.Model):
    store = models.CharField(max_length=100, choices=CHOICES_STORES)
    count = models.PositiveIntegerField(default=0)
    updated = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.store + ' - ' + self.updated.isoformat()


class Chart(models.Model):
    sale = models.ForeignKey('Sale')
    price_old = models.FloatField(default=0)
    price_now = models.FloatField(default=0)
    discount = models.PositiveIntegerField(default=0)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.sale.store + ' - ' + self.sale.name


# http://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml