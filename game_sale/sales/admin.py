from django.contrib import admin
from sales.models import Sale, Store, Chart


class SaleAdmin(admin.ModelAdmin):
    list_display = ('id', 'updated', 'store', 'name', 'name_real', 'discount', 'is_active', 'is_bundle', 'is_dlc')
    search_fields = ('name', 'name_real',)
    list_filter = ('store', 'is_active', 'is_bundle', 'is_dlc')


class StoreAdmin(admin.ModelAdmin):
    list_display = ('store', 'count', 'updated')


class ChartAdmin(admin.ModelAdmin):
    list_display = ('sale', 'price_now', 'created')


admin.site.register(Sale, SaleAdmin)
admin.site.register(Store, StoreAdmin)
admin.site.register(Chart, ChartAdmin)