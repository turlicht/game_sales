# -*- coding: utf-8 -*-
from collections import defaultdict, OrderedDict
from django.db.models import Q
from django.views.generic import ListView, DetailView
import time
from django.forms.models import model_to_dict
import datetime
from backend.views import BackendView
from parser.parse import STORES
from parser.tasks import sales_parse, search_parse, search_store_parse
from sales.forms import MainSearchForm
from sales.models import Sale, Store, Chart
from utils.filters_form_mixin import FiltersFormMixin
from django.utils.timezone import utc

STORES_LIST = STORES.keys()


class ParseView(BackendView):
    """
    Ручной запуск парсера скидок
    """
    def get(self, request, store_name=None):
        sales_parse(store_name)
        return self.response([])


class SleepView(BackendView):
    """

    """
    def get(self, request):
        time.sleep(60 * 5)
        return self.response([])


class StoresView(BackendView):
    """
    Список магазинов и их последних обновлений
    """
    def get(self, request):
        result = []
        for store_name, info in STORES.items():
            store = Store.objects.get_or_create(store=store_name)[0]
            info['id'] = store_name
            info['updated'] = time.mktime(store.updated.timetuple())
            info['count'] = store.count
            result.append(info)
        return self.response(result)


class SalesView(BackendView):
    """
    Получить все скидки из магазина. В url передать название магазина
    """
    @staticmethod
    def get_sales(store_name):
        sales = Sale.objects.filter(store=store_name).filter(is_active=True)
        results = []
        for sale in sales:
            sale.price_old_rub = sale.price_old
            sale.price_now_rub = sale.price_now

            results.append({
                'store': store_name,
                'id': sale.id,
                'name': sale.name,
                'store_id': sale.id,
                'link': sale.link,
                'image': sale.image,
                'price_old_rub': sale.price_old_rub,
                'price_now_rub': sale.price_now_rub,
                'discount': sale.discount,
                'updated': time.mktime(sale.updated.timetuple()),
                'price_updated': time.mktime(sale.price_updated.timetuple()),

                'is_bundle': sale.is_bundle,
                'is_dlc': sale.is_dlc,
            })
        return results

    def get(self, request, store_name):
        if store_name in STORES_LIST:
            return self.response(self.get_sales(store_name))
        return self.response_error(100, 'Unknown store')


class BestView(BackendView):
    """
    Получить список самых больших скидок

    Параметры:
        limit
        skip
        min_price_old_rub
    """
    def get(self, request):
        try:
            limit = int(request.GET.get('limit', 10))
        except ValueError:
            limit = 10
        try:
            min_price_old_rub = int(request.GET.get('min_price_old_rub', 0))
        except ValueError:
            min_price_old_rub = 0
        try:
            skip = int(request.GET.get('skip', 0))
        except ValueError:
            skip = 0

        sales = Sale.objects.order_by('-discount').filter(price_old_rub__gte=min_price_old_rub)[skip:skip+limit]
        sales = [model_to_dict(sale) for sale in sales]
        for sale in sales:
            del sale['store_type']
            del sale['genre']
            del sale['platforms']
            del sale['release']
        return self.response(sales)


class SearchView(BackendView):
    """
    Поиск игр в магазинах. В url передать название магазина

    Параметры:
        query   - текст запроса не менее 3-х символов
    """
    def get(self, request, store_name):
        if not store_name in STORES_LIST:
            return self.response_error(100, 'Unknown store')

        query = request.GET.get('query', '')
        games = []
        if len(query) > 3:
            games = search_store_parse(store_name, query)
        return self.response(games)


class MainView(FiltersFormMixin, ListView):
    template_name = 'sales/main.html'
    success_url = '/'
    context_object_name = 'sales'
    paginate_by = 50
    form_class = MainSearchForm

    def get_queryset(self):
        sale = Sale.objects
        filters = self.get_filters()
        if 'stores' in filters:
            sale = sale.filter(store__in=filters['stores'])
        if 'name' in filters:
            sale = sale.filter(name__icontains=filters['name'])
        if 'price_from' in filters:
            sale = sale.filter(price_now_rub__gte=filters['price_from'])
        if 'price_to' in filters:
            sale = sale.filter(price_now_rub__lte=filters['price_to'])
        sale = sale.filter(is_active=True).filter(is_bundle=False).filter(expire__gt=datetime.datetime.now().replace(tzinfo=utc))
        return sale.order_by('is_dlc', '-discount')

    def get_context_data(self, **kwargs):
        context = super(MainView, self).get_context_data(**kwargs)
        if self.request.is_mobile:
            page_extension = 3
        else:
            page_extension = 5

        pages = context['paginator'].num_pages
        page = context['page_obj'].number
        start_page = page - page_extension
        end_page = page + page_extension
        if start_page < 1:
            end_page += -start_page + 1
            start_page = 1

        if end_page > pages:
            start_page -= end_page - pages - 1
            if start_page < 1:
                start_page = 1
            end_page = pages + 1

        context['page_range'] = [i for i in range(start_page, end_page)]
        for sale in context['sales']:
            sale.store_name_view = STORES.get(sale.store, {'name': ''})['name']
            sale.store_site_view = STORES.get(sale.store, {'site': ''})['site']
            sale.currency = {'rub': 'руб.', 'usd': 'USD'}[STORES.get(sale.store, {'currency': 'usd'})['currency']]
            sale.image = sale.image[:-4] + '_h48.jpg'
        return context


class SaleView(DetailView):
    template_name = 'sales/sale.html'
    model = Sale

    def get_context_data(self, **kwargs):
        context = super(SaleView, self).get_context_data(**kwargs)
        sale = context['sale']
        if not sale.name_real:
            sale.name_real = sale.name
        charts = Chart.objects.filter(Q(sale__name=sale.name_real) |
                                      Q(sale__name_real=sale.name_real)
                                      ).order_by('created')
        dates = []

        first_date = charts[0].created
        now = datetime.datetime.now().replace(tzinfo=utc) + datetime.timedelta(days=1)
        i = -3
        while True:
            date = first_date + datetime.timedelta(days=i)
            if date > now:
                break
            date = date.strftime('%d-%m-%Y')
            dates.append(date)
            i += 1

        datas = defaultdict(dict)
        sales = set()
        for chart in charts:
            sales.add(chart.sale.id)
            date = chart.created.strftime('%d-%m-%Y')
            # Из одного магазина может прийти две одинаковые игры, нужно взять меньшую цену
            if date in datas[chart.sale.store]:
                if chart.price_now < datas[chart.sale.store][date]['price_now']:
                    datas[chart.sale.store][date] = {'price_old': chart.price_old,
                                                     'price_now': chart.price_now,
                                                     'price': chart.price_now}
            else:
                datas[chart.sale.store][date] = {'price_old': chart.price_old,
                                                 'price_now': chart.price_now,
                                                 'price': chart.price_now}

        for store, data in datas.items():
            old_price = None
            for date in reversed(dates):
                if not date in data and old_price is None:
                    continue
                elif not date in data:
                    data[date] = {'price': old_price}
                else:
                    old_price = data[date]['price_old']

        series = []
        for store, data in datas.items():
            sery = {
                'name': STORES[store]['name'],
                'color': STORES[store]['color'],
                'data': []
            }
            for date in dates:
                if date in data:
                    sery['data'].append('[Date.UTC(%d, %d, %d), %d]' %
                                        (
                                            int(date.split('-')[2]),
                                            int(date.split('-')[1]) - 1,
                                            int(date.split('-')[0]),
                                            data[date]['price']
                                        )
                                        )
            sery['data'] = ','.join(sery['data'])
            series.append(sery)

        sales = Sale.objects.filter(id__in=sales)
        for sale in sales:
            sale.store_name_view = STORES.get(sale.store, {'name': ''})['name']
            sale.store_site_view = STORES.get(sale.store, {'site': ''})['site']
        context['sales'] = sales
        context['series'] = series
        return context