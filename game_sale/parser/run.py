import os


if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "game_sale.settings")
    from tasks import sales_parse
    import django
    django.setup()

    sales_parse()
    exit(0)