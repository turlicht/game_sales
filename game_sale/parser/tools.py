import re


def get_prices(price_now, price_old=None, discount=None):
    if not price_old and not discount:
        return None, None, None

    price_now = re.findall(r'(\d+)', price_now)
    if not price_now:
        return None, None, None
    price_now = float(price_now[0])

    if price_old:
        price_old = re.findall(r'(\d+)', price_old)
        if not price_old:
            return None, None, None
        price_old = float(price_old[0])

        discount = 100 - int(price_now / price_old * 100)
        return price_now, price_old, discount

    return None, None, None
