from django.contrib import admin
from parser.models import ParseError, ParseResult


class ParseErrorAdmin(admin.ModelAdmin):
    list_display = ('type', 'store', 'error', 'updated',)
    search_fields = ('error',)
    list_filter = ('type', 'store', 'updated',)


class ParseResultAdmin(admin.ModelAdmin):
    list_display = ('result', 'updated',)
    list_filter = ('updated',)


admin.site.register(ParseError, ParseErrorAdmin)
admin.site.register(ParseResult, ParseResultAdmin)
