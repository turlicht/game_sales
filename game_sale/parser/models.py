from django.db import models
from parser.parse import STORES

CHOICES_TYPES = [('sales', 'sales'), ('search', 'search')]
CHOICES_STORES = [(s, s) for s in STORES]


class ParseError(models.Model):
    type = models.CharField(max_length=100, choices=CHOICES_TYPES)
    store = models.CharField(max_length=100, choices=CHOICES_STORES)
    error = models.CharField(max_length=255, blank=True)
    url = models.CharField(max_length=255, blank=True)
    traceback = models.TextField(blank=True)
    code = models.TextField(blank=True)
    updated = models.DateTimeField(auto_now=True)


class ParseResult(models.Model):
    result = models.TextField()
    updated = models.DateTimeField(auto_now=True)
