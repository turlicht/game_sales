# -*- coding: utf-8 -*-
from collections import defaultdict
import json
import re
import requests
from lxml import html, etree
import time
from parser.tools import get_prices

STORES = {
    'steam_ru':     {'name': 'Steam',         'currency': 'rub', 'language': 'ru', 'color': '#000000', 'site': 'http://store.steampowered.com/'},
    'humblebundle': {'name': 'Humble Bundle', 'currency': 'usd', 'language': 'en', 'color': '#AAAAAA', 'site': 'https://www.humblebundle.com/'},
    'gamazavr':     {'name': 'Гамазавр',      'currency': 'rub', 'language': 'ru', 'color': '#CC9900', 'site': 'http://gamazavr.ru/'},
    'daigama':      {'name': 'DaiGama',       'currency': 'rub', 'language': 'ru', 'color': '#FF9900', 'site': 'http://store.daigama.ru/'},
    'gamagama':     {'name': 'Gama-Gama',     'currency': 'rub', 'language': 'ru', 'color': '#33FF33', 'site': 'http://gama-gama.ru/'},
    'steampay':     {'name': 'Steampay',      'currency': 'rub', 'language': 'ru', 'color': '#66CCFF', 'site': 'http://steampay.com/'},
    'zakazaka':     {'name': 'Zaka-Zaka',     'currency': 'rub', 'language': 'ru', 'color': '#3300FF', 'site': 'http://zaka-zaka.com/'},
    'playo':        {'name': 'Playo.ru',      'currency': 'rub', 'language': 'ru', 'color': '#000066', 'site': 'http://playo.ru/'},
}  # http://cssonline.net.ru/colors.php

TRACE_INFO = defaultdict(str)


def sales_parse_steam_ru():
    """
<a href="http://store.steampowered.com/app/238320/?snr=1_7_7_204_150_1"  data-ds-appid="238320" onmouseover="GameHover( this, event, $('global_hover'), {&quot;type&quot;:&quot;app&quot;,&quot;id&quot;:&quot;238320&quot;,&quot;public&quot;:1,&quot;v6&quot;:1} );" onmouseout="HideGameHover( this, event, $('global_hover') )" class="search_result_row ds_collapse_flag" >
    <div class="col search_capsule"><img src="http://cdn.akamai.steamstatic.com/steam/apps/238320/capsule_sm_120.jpg?t=1399595107" alt="Buy Outlast" width="120" height="45"></div>
    <div class="col search_name ellipsis">
        <span class="title">Outlast</span>
        <p><span class="platform_img win"></span></p>
    </div>
    <div class="col search_released">4 сен, 2013</div>
    <div class="col search_reviewscore">
        <span class="search_review_summary positive" data-store-tooltip="Крайне положительные&lt;br&gt;97% из 11,229 обзоров этой игры положительны.">
        </span>
    </div>
    <div class="col search_discount">
        <span>-66%</span>
    </div>
    <div class="col search_price discounted">
        <span style="color: #888888;"><strike>419 p&#1091;&#1073;.</strike></span><br>142 p&#1091;&#1073;.
    </div>
</a>
    """
    sale_types = [
        {'is_dlc': False, 'is_bundle': False, 'category1': '998'},
        {'is_dlc': True,  'is_bundle': False, 'category1': '21'},
        {'is_dlc': False, 'is_bundle': True,  'category1': '996'},
    ]
    results = []
    for sale_type in sale_types:
        for page_num in range(100):
            TRACE_INFO['url'] = 'http://store.steampowered.com/search/?snr=1_4_4__12&cc=ru&l=russian&specials=1&category1=%s&sort_order=ASC&page=%s' % \
                                (sale_type['category1'], str(page_num + 1))
            time.sleep(1)
            r = requests.get(TRACE_INFO['url'])
            page = html.document_fromstring(r.text)
            TRACE_INFO['code'] = etree.tostring(page, pretty_print=True)
            sales = page.cssselect('a.search_result_row')
            if not sales:
                break

            for sale in sales:
                TRACE_INFO['code'] = etree.tostring(sale, pretty_print=True)
                is_dlc = sale_type['is_dlc']
                is_bundle = sale_type['is_bundle']

                image = sale.cssselect('div.search_capsule')[0][0].attrib['src']
                game_id = re.search(r"/steam/\w+/(\d+)/", image).group(1)
                game_type = re.search(r"/steam/(\w+)/\d+/", image).group(1)

                if not sale.cssselect('strike'):
                    continue

                price_old = sale.cssselect('strike')[0].text_content()
                price_now = sale.cssselect('div.search_price')[0].text_content().replace(price_old, '')
                price_now, price_old, discount = get_prices(price_now, price_old)
                if not discount:
                    continue

                results.append({
                    'name': sale.cssselect('span.title')[0].text_content().strip(),
                    'link': sale.attrib['href'],
                    'image': [
                        'http://cdn.akamai.steamstatic.com/steam/%s/%s/header.jpg' % (game_type, game_id),
                        'http://cdn.akamai.steamstatic.com/steam/%s/%s/header_292x136.jpg' % (game_type, game_id),
                        'http://cdn.akamai.steamstatic.com/steam/%s/%s/header_586x192.jpg' % (game_type, game_id),
                        'http://cdn.akamai.steamstatic.com/steam/%s/%s/capsule_616x353.jpg' % (game_type, game_id),
                        image.split('?')[0],
                    ],
                    'price_old': price_old,
                    'price_now': price_now,
                    'discount': discount,
                    'is_bundle': is_bundle,
                    'is_dlc': is_dlc,
                })
    return results


def search_parse_steam_ru(query):
    """
<a href="http://store.steampowered.com/app/620/?snr=1_7_7_151_150_1" class="search_result_row even" >
    <div class="col search_price">399 p&#1091;&#1073;.</div>
    <div class="col search_type">
        <img src="http://store.akamai.steamstatic.com/public/images/ico/ico_type_app.gif">
    </div>
    <div class="col search_metascore">95</div>
    <div class="col search_released">19 Apr 2011</div>
    <div class="col search_capsule"><img src="http://cdn.akamai.steamstatic.com/steam/apps/620/capsule_sm_120.jpg?t=1405713503" alt="Buy Portal 2" width="120" height="45"></div>
    <div class="col search_name ellipsis">
        <h4>Portal 2</h4>
        <p>
            <span class="platform_img steamplay"></span>
            <span class="platform_img win"></span>
            <span class="platform_img mac"></span>
            <span class="platform_img linux"></span>Action, Adventure - Released: 19 Apr 2011
        </p>
    </div>
    <div style="clear: both;"></div>
</a
    """
    TRACE_INFO['steam_ru']['url'] = 'http://store.steampowered.com/search/?snr=1_4_4__12&cc=ru&l=russian&term=%s' % query
    r = requests.get(TRACE_INFO['steam_ru']['url'])
    page = html.document_fromstring(r.text)
    TRACE_INFO['code'] = etree.tostring(page, pretty_print=True)
    games = page.cssselect('a.search_result_row')
    results = []
    for game in games:
        TRACE_INFO['steam_ru']['code'] = etree.tostring(game, pretty_print=True)
        name = game.cssselect('h4')[0].text_content()
        if not query in name.lower():
            continue

        search_type = game.cssselect('div.search_type')[0][0].attrib['src']
        if search_type != 'http://store.akamai.steamstatic.com/public/images/ico/ico_type_app.gif':
            continue

        link = game.attrib['href']
        price = game.cssselect('div.search_price')[0].text_content()
        #price = float(re.findall(r'(\d+)', price)[0])

        results.append({
            #'store_id': game_id,
            'name': name.strip(),
            'link': link,
            'image': game.cssselect('div.search_capsule')[0][0].attrib['src'].replace('capsule_sm_120', 'header_292x136'),
            'price_rub': price,
        })
    return results


def sales_parse_gamazavr():
    """
<div class="item">
    <div class="price">
        <s>599<span>&nbsp;</span></s>
        <b>149</b>&nbsp;руб
    </div>
    <a href="/product/14681295/" title="BioShock Infinite" class="img"><img src="/media/products/profile/8689/300_135_1.jpg/thumb198/" alt="BioShock Infinite" width="198" height="89"/></a>
    <div class="description">
        <a href="/product/14681295/" title="BioShock Infinite"><b>BioShock Infinite</b></a>
        <p>Жанры: Action и Шутеры</p>
        <p>...</p>
    </div>
</div>
    """

    TRACE_INFO['url'] = 'http://gamazavr.ru/category/promo/?page=1&sort=order_top&ppage=100'
    r = requests.get(TRACE_INFO['url'])
    r.encoding = 'UTF-8'

    page = html.document_fromstring(r.text)
    TRACE_INFO['code'] = etree.tostring(page, pretty_print=True)
    sales = page.cssselect('div.productsList')[0]
    results = []
    for n, sale in enumerate(sales):
        TRACE_INFO['code'] = etree.tostring(sale, pretty_print=True)

        price_old = sale.cssselect('div.price')[0][0].text_content()
        price_now = sale.cssselect('div.price')[0][1].text_content()
        price_old = float(re.findall(r'(\d+)', price_old)[0])
        price_now = float(re.findall(r'(\d+)', price_now)[0])
        discount = 100 - int(price_now / price_old * 100)

        results.append({
            'name': sale.cssselect('div.description')[0][0].text_content().strip(),
            'link': 'http://gamazavr.ru' + sale[1].attrib['href'],
            'image': 'http://gamazavr.ru' + sale[1][0].attrib['src'].replace('/thumb198/', ''),

            'price_old': price_old,
            'price_now': price_now,
            'discount': discount,
        })
    return results


def sales_parse_daigama():
    """
<li>
    <div class="content">
        <div class="visual">
            <a href="/games/wolfenstein-new-order/?product=326616459"><img src="/media/games/wolfenstein-new-order/wolfenstein-new-order-box-medium.jpg" width="140" height="197"/></a>
            <a class="link" href="/games/wolfenstein-new-order/?product=326616459"><span class="icon"><span/></span></a>
        </div>
        <strong class="title"><a href="/games/wolfenstein-new-order/?product=326616459">Wolfenstein: The New ...</a></strong>
        <div class="edition">&#1057;&#1090;&#1072;&#1085;&#1076;&#1072;&#1088;&#1090;&#1085;&#1086;&#1077;</div>
        <span class="final-price">569 &#1088;&#1091;&#1073;.</span>
        <span class="price">599 &#1088;&#1091;&#1073;.</span>
    </div>
</li>
    """
    i = 0
    next_page = 1
    page = ''
    while True:
        TRACE_INFO['url'] = 'http://store.daigama.ru/search/more/?ajax=1&has_deal=1&page=%s' % str(next_page)
        r = requests.get(TRACE_INFO['url'])
        time.sleep(1)
        response_json = json.loads(r.text)
        page += response_json['html']
        if response_json['next_page']:
            next_page = response_json['next_page']
        else:
            break

        i += 1
        if i == 100:
            break

    page = html.document_fromstring(page)
    results = []
    for n, sale in enumerate(page[0]):
        link = 'http://store.daigama.ru' + sale.cssselect('strong.title')[0][0].attrib['href']
        TRACE_INFO['url'] = link
        r = requests.get(link)
        time.sleep(1)
        sale_page = html.document_fromstring(r.text)
        TRACE_INFO['code'] = etree.tostring(sale_page, pretty_print=True)
        sale_info = sale_page.cssselect('div.product-card')[0]
        TRACE_INFO['code'] = etree.tostring(sale_info, pretty_print=True)
        name = sale_info[1][0].text_content()

        TRACE_INFO['code'] = etree.tostring(sale, pretty_print=True)
        price_old = sale.cssselect('span.price')[0].text_content().replace(' ', '')
        price_now = sale.cssselect('span.final-price')[0].text_content().replace(' ', '')
        price_old = float(re.findall(r'(\d+)', price_old)[0])
        price_now = float(re.findall(r'(\d+)', price_now)[0])
        discount = 100 - int(price_now / price_old * 100)

        results.append({
            'name': name.strip(),
            'link': link,
            'image': 'http://store.daigama.ru' + sale.cssselect('div.visual')[0][0][0].attrib['src'].replace('-medium', ''),

            'price_old': price_old,
            'price_now': price_now,
            'discount': discount,
        })
    return results


def sales_parse_gamagama():
    """
<a href="//gama-gama.ru/detail/saints_row_2/" title="&#x41A;&#x443;&#x43F;&#x438;&#x442;&#x44C; Saints Row 2">
    <div class="catalog-row js-tooltip-toggle" data-id="45400">
        <div class="w-mic_game">
            <img alt="&#1050;&#1091;&#1087;&#1080;&#1090;&#1100; Saints Row 2" title="&#1050;&#1091;&#1087;&#1080;&#1090;&#1100; Saints Row 2" class="mic_game" src="http://s.gama-gama.ru/resize/secure/1ba01d969e901a746e6aaae93cd787e3/crop/156x61/fullsize/f555f07890277f7c0ee5ad8ebef5df52.jpg"/>
        </div>
        <div class="catalog-product-info">
            <div class="catalog_name"><span class="cropable">Saints Row 2</span></div>
            <div class="catalog_desc bigcatalog">Deep Silver</div>
            <div class="catalog_req bigcatalog">
            <span>PC</span> &#9679;
            <span class="red">
                &#1069;&#1082;&#1096;&#1085;, 																								Steam															</span>
                <div class="catalog_full_desc">
                    &#1040;&#1073;&#1089;&#1091;&#1088;&#1076;, &#1102;&#1084;&#1086;&#1088;, &#1085;&#1077;&#1075;&#1088;&#1099; - &#1095;&#1090;&#1086; &#1077;&#1097;&#1105; &#1085;&#1072;&#1076;&#1086; &#1076;&#1083;&#1103; &#1074;&#1077;&#1089;&#1105;&#1083;&#1086;&#1081; &#1080;&#1075;&#1088;&#1099; ?
                </div>
            </div>
        </div>
        <div class="catalog_price_holder">
            <div class="price_2">
                <div class="price_discount">-50%</div>
                <div class="price_group">
                    <div class="old_price">99 &#1088;&#1091;&#1073;.</div>
                    <div class="promo_price">49 &#1088;&#1091;&#1073;.</div>
                </div>
            </div>
            <div style="display:none;" class="price_in_bonuses">
                6 277
                <img alt="&#1073;&#1086;&#1085;&#1091;&#1089;&#1099;" class="gama-bonus-icon" src="http://s.gama-gama.ru/style/gama-gama/img/pricev2_bonus_small.png"/>
            </div>
        </div>
        <div class="game_hover_card right-side js-card-tooltip">
        <div class="game_hover_card_div_1">
            <div class="game_hover_card_div_2">
                <div class="catalog_name">
                    Saints Row 2
                </div>
                <div>
                      &#1040;&#1073;&#1089;&#1091;&#1088;&#1076;, &#1102;&#1084;&#1086;&#1088;, &#1085;&#1077;&#1075;&#1088;&#1099; - &#1095;&#1090;&#1086; &#1077;&#1097;&#1105; &#1085;&#1072;&#1076;&#1086; &#1076;&#1083;&#1103; &#1074;&#1077;&#1089;&#1105;&#1083;&#1086;&#1081; &#1080;&#1075;&#1088;&#1099; ?
                </div>
                <div>
                    <br/>&#1042; &#1087;&#1088;&#1086;&#1076;&#1072;&#1078;&#1077; &#1089;: 28 &#1103;&#1085;&#1074;&#1072;&#1088;&#1103; 2009<br/>&#1048;&#1079;&#1076;&#1072;&#1090;&#1077;&#1083;&#1100;: Deep Silver<br/>&#1056;&#1072;&#1079;&#1088;&#1072;&#1073;&#1086;&#1090;&#1095;&#1080;&#1082;: Volition<br/>&#1069;&#1082;&#1096;&#1085;, Steam<br/>			</div>
            </div>
        </div>
        <img src="http://s.gama-gama.ru/style/gama-gama/img/helper_arrow.png" alt="" class="game_hover_card_arrow js-card-tooltip-arrow"/>
        </div>
    </div>
</a>
    """
    TRACE_INFO['url'] = 'http://gama-gama.ru/games/?genre2=actions'
    r = requests.get(TRACE_INFO['url'])
    # r.encoding = 'UTF-8'
    page = html.document_fromstring(r.text)
    TRACE_INFO['code'] = etree.tostring(page, pretty_print=True)
    sales = page.cssselect('div.catalog-content')[0]
    results = []
    for n, sale in enumerate(sales):
        TRACE_INFO['code'] = etree.tostring(sale, pretty_print=True)

        link = 'http:' + sale.attrib['href']
        price_old = sale.cssselect('div.old_price')[0].text_content().replace(' ', '')
        price_now = sale.cssselect('div.promo_price')[0].text_content()
        price_old = float(re.findall(r'(\d+)', price_old)[0])
        price_now = float(re.findall(r'(\d+)', price_now)[0])
        discount = 100 - int(price_now / price_old * 100)
        if discount <= 0:
            continue

        results.append({
            'name': sale.cssselect('div.game_hover_card_div_2')[0][0].text_content().strip(),
            'link': link,
            'image': sale.cssselect('img.mic_game')[0].attrib['src'],

            'price_old': price_old,
            'price_now': price_now,
            'discount': discount,
        })
    return results


def sales_parse_steampay():
    """
<li>
    <a style="height: 60px;" href="/game/war-of-the-roses-kingmaker">
        <span class="photo"><img width="131" src="http://steampay.com/image/war-of-the-roses.jpg" alt="photo"></span>
        <span class="main" style="width: 325px;">
            <span class="name">War of the Roses: Kingmaker</span>
            <span class="category"><img src="/template/images/list_products/ico/name_2.png" alt="category"></span>
        </span>
        <span class="cost">165 <img src="/template/images/ico/rubl-gray.png" alt="rubl"></span>
        <div style="..."><div style="...">50%</div></div>
    </a>
</li>
    """
    page = ''
    for i in range(4):
        TRACE_INFO['url'] = 'http://steampay.com/get.php?curr=wmr&sort=reliz&order=desc&event=1&page=%s' % str(i)
        r = requests.get(TRACE_INFO['url'])
        page += r.text
        time.sleep(3)

    page = html.document_fromstring(page)
    TRACE_INFO['code'] = etree.tostring(page, pretty_print=True)
    sales = page.cssselect('a')
    results = []
    for n, sale in enumerate(sales):
        TRACE_INFO['code'] = etree.tostring(sale, pretty_print=True)

        price_now = sale.cssselect('span.cost')
        if not price_now:
            continue
        price_now = float(price_now[0].text_content())
        discount = sale[3][0].text_content()
        discount = int(re.findall(r'(\d+)', discount)[0])
        #discount = 100 - int(price_now / price_old * 100)
        price_old = int(price_now / (100 - discount) * 100)

        image = sale.cssselect('span.photo')[0][0].attrib['src']
        if image[:4] != 'http':
            image = 'http://steampay.com' + image

        results.append({
            'name': sale.cssselect('span.name')[0].text_content(),
            'link': 'http://steampay.com' + sale.attrib['href'],
            'image': image,

            'price_old': price_old,
            'price_now': price_now,
            'discount': discount,
        })
    return results


def sales_parse_zakazaka():
    """
<UL class="games">
<LI class="odd">
 <A href="/game/borderlands-the-pre-sequel">
  <IMG src="/i/game/borderlands-the-pre-sequel_218.jpg" alt="Borderlands: The Pre-Sequel">
  <H1><SPAN>Borderlands: The Pre-Sequel</SPAN></H1>
  <P>990<SPAN>р.</SPAN></P><DIV>-18<SPAN>%</SPAN></DIV>
 </A>
</LI>
    """
    results = []
    is_first = True
    for i in range(1, 100):
        if is_first:
            TRACE_INFO['url'] = 'http://zaka-zaka.com/game/sale/'
            is_first = False
        else:
            TRACE_INFO['url'] = 'http://zaka-zaka.com/game/sale/page%s' % str(i)
        r = requests.get(TRACE_INFO['url'])
        r.encoding = 'UTF-8'

        page = html.document_fromstring(r.text)
        TRACE_INFO['code'] = etree.tostring(page, pretty_print=True)
        sales = page.cssselect('ul.games')[0]

        for n, sale in enumerate(sales):
            TRACE_INFO['code'] = etree.tostring(sale, pretty_print=True)

            price_now = sale.cssselect('p')[0].text_content()
            price_now = float(re.findall(r'(\d+)', price_now)[0])
            discount = sale.cssselect('div')[0].text_content()
            discount = int(re.findall(r'(\d+)', discount)[0])
            #discount = 100 - int(price_now / price_old * 100)
            price_old = int(price_now / (100 - discount) * 100)

            results.append({
                'name': sale.cssselect('h1')[0].text_content().strip(),
                'link': 'http://zaka-zaka.com' + sale.cssselect('a')[0].attrib['href'],
                'image': 'http://zaka-zaka.com' + sale.cssselect('img')[0].attrib['src'],

                'price_old': price_old,
                'price_now': price_now,
                'discount': discount,
            })

        time.sleep(1)
        if len(sales) < 11 or i > 100:
            break

    return results


# def sales_parse_playo():
#     """
#     <div class="main_page_game_item">
#         <a href="/goods/280/"><img src="//452660689.r.cdn.skyparkcdn.ru/files/t.3583_1402421925.jpg" alt="купить FIFA 15" class="main_page_top_img"/></a><br />
#         <div class="main_game_price" onclick="window.location = '/goods/280/'">
#             <a class="main_game_price" href="/goods/280/" style="text-decoration: none;"><span class="old_price_through">
#                 <span class="old_price">1499</span></span>1395р.
#             </a>
#         </div>
#         <a href="/goods/280/" class="game_name">FIFA 15</a>
#     </div>
#     """
#     TRACE_INFO['url'] = 'http://playo.ru/'
#     r = requests.get(TRACE_INFO['url'])
#     r.encoding = 'UTF-8'
#     page = html.document_fromstring(r.text)
#     TRACE_INFO['code'] = etree.tostring(page, pretty_print=True)
#     sales = page.cssselect('div#top_goods_best_cnt')[0]
#     results = []
#     for n, sale in enumerate(sales):
#         TRACE_INFO['code'] = etree.tostring(sale, pretty_print=True)
#
#         if not sale.cssselect('div.old_price'):
#             continue
#
#         price_old = sale.cssselect('div.old_price')[0].text_content()
#         price_now = sale.cssselect('div.price')[0].text_content().replace(price_old, '')
#         price_old = float(re.findall(r'(\d+)', price_old)[0])
#         price_now = float(re.findall(r'(\d+)', price_now)[0])
#         discount = 100 - int(price_now / price_old * 100)
#         if discount <= 0:
#             continue
#
#         results.append({
#             'name': sale.cssselect('div.inf')[0].text_content().strip(),
#             'link': 'http://playo.ru' + sale.cssselect('a.link_preview')[0].attrib['href'],
#             'image': 'http:' + sale.cssselect('div.img_prev')[0][0].attrib['src'],
#
#             'price_old': price_old,
#             'price_now': price_now,
#             'discount': discount,
#         })
#     if not results:
#         raise Exception('Sales not found')
#     return results


def sales_parse_playo():
    """
<div class=\"preview_it\">
    <a href=\"/goods/294/\" class=\"link_preview\">
        <div class=\"img_prev\">
            <img src=\"//452660689.r.cdn.skyparkcdn.ru/files/t.9476_1405349534.png\"  alt=\"рандом steam\" title=\"рандом steam\" />
            <div class=\"game_txt\">
                <div class=\"gmlst_dscnt_lbl\" style=\"background-color:  !important;\">-51%</div>
                <div class=\"inf\">\n\t\t\t\t\t\tКупить Steam ключ рандом, испытай удачу!\n\t\t\t\t\t\t<br /><span class=\"gmlst_dsnt_val_text\">с экономией 50 &#8377;</span>\t\t\t\t\t</div>
            </div>
        </div>
        <div class=\"buy_info\">
            <div class=\"price_bl\">
                <div class=\"price\">49 руб.</div>
                <div class=\"old_price\">99 руб.</div>
            </div>
        </div>
    </a>
    <a href=\"/goods/294/buy/\" class=\"buy_this\"></a>
</div>
    """
    TRACE_INFO['url'] = 'http://playo.ru/'
    headers = {'content-type': 'application/x-www-form-urlencoded; charset=UTF-8'}
    results = []

    for i in [0, 20, 40, 60, 80, 100]:
        r = requests.post(TRACE_INFO['url'], 'action=loadMPGames_Sale&start=%s&form_id=top_goods_sale_frm' % i,
                          headers=headers)
        data = json.loads(r.text)
        count = data['count']
        page = data['html']
        if not page:
            break
        page = html.document_fromstring(page)
        sales = page.cssselect('div.preview_it')
        for n, sale in enumerate(sales):
            TRACE_INFO['code'] = etree.tostring(sale, pretty_print=True)

            if not sale.cssselect('div.old_price'):
                continue

            price_old = sale.cssselect('div.old_price')[0].text_content()
            price_now = sale.cssselect('div.price')[0].text_content().replace(price_old, '')
            price_old = float(re.findall(r'(\d+)', price_old)[0])
            price_now = float(re.findall(r'(\d+)', price_now)[0])
            discount = 100 - int(price_now / price_old * 100)
            if discount <= 0:
                continue

            results.append({
                'name': sale.cssselect('div.inf')[0].text_content().strip().split('\n')[0].replace('Купить ', ''),
                'link': 'http://playo.ru' + sale.cssselect('a.link_preview')[0].attrib['href'],
                'image': 'http:' + sale.cssselect('div.img_prev')[0][0].attrib['src'],

                'price_old': price_old,
                'price_now': price_now,
                'discount': discount,
            })

    if not results:
        raise Exception('Sales not found')
    return results


def sales_parse_humblebundle():
    """
{
    "machine_name": "satellitereign_storefront",
    "alert_messages": [
        {
            "color": "green",
            "text": "Pre-order Satellite Reign now! "
        }
    ],
    "storefront_featured_image_small": "/misc/files/hashed/0cfeaac15086a99be43e9c3d876f8d1ae7f0772b.jpg",
    "youtube_link": "wDJDBgos0Go",
    "platforms": [
        "windows",
        "mac",
        "linux"
    ],
    "promotional_message": null,
    "usk_rating": "",
    "force_popup": true,
    "rating_details": null,
    "esrb_rating": "",
    "developers": [
        {
            "developer-name": "5 Lives Studios",
            "developer-url": "http://5livesstudios.com/"
        }
    ],
    "publishers": null,
    "delivery_methods": [
        "download"
    ],
    "description": "<p><strong>Satellite Reign is a real-time, class-based strategy game </strong><span>from the creator of Syndicate Wars</span><span>.</span><span> You control a team of four agents, each with </span><strong>distinct and unique abilities</strong><span>, collectively battling for control of a </span><strong>fully simulated, living cyberpunk city.</strong></p>\n<p><span>The game world is designed to facilitate </span><strong>emergent gameplay</strong><span>, giving you the</span><span><strong> tools and freedom to play how you want to play</strong>, </span><span>so you can create strategies and scenarios that not even </span><span>we</span><span> had anticipated!</span></p>\n<p><span>Customise your team with the strength to </span><strong>destroy your enemies head-on</strong><span>, or hack into their facilities to </span><strong>manipulate their infrastructure </strong><span>without them ever knowing you were even there.</span></p>\n<p><span>Will you take down your enemies with </span><strong>brute-force</strong><span>? Covert espionage and </span><strong>infiltration</strong><span>? Or will you use </span><strong>propaganda </strong><span>to influence the citizens of the city and overthrow the controlling powers?</span></p>\n<p><span>The world's governments are controlled by mega-corporations, democracy to the highest bidder. Society is structured for the benefit of those in power. The poor exist in the dark squalid underbelly of the city, while the wealthy swim in opulence and luxury on the upper tiers, and the vast middle-class are too comfortable with their lives of convenience to see the world for what it really is. Corporate police patrol the streets, brutally maintaining the status quo, all under the guise of keeping the people safe.</span></p>\n<p><span>The time for change is now, as a mysterious organisation rises from the slums of the city. They'll have to bribe, steal, hack, kill and augment their way through the barriers between them and their ultimate goal...</span><strong>but what are they trying to achieve?</strong><span> &nbsp;To free the masses from the corporate stranglehold, or to take control for themselves? &nbsp;</span><strong>That, is up to you.</strong></p>",
    "allowed_territories": null,
    "minimum_age": null,
    "system_requirements": "<p><strong>Minimum:</strong><br /><br /><span style=\"text-decoration: underline;\"><strong>Windows</strong></span><br /><strong>OS:</strong>&nbsp;Windowx&nbsp;7/8<br /><strong>Processor:</strong>&nbsp;Dual Core 2Ghz&nbsp;<br /><strong>Memory:</strong>&nbsp;2 GB RAM&nbsp;<br /><strong>Graphics:</strong>&nbsp;1GB &nbsp;DX9 or equivalent OpenGL GPU<br /><br /><span style=\"text-decoration: underline;\"><strong>Mac</strong></span><br /><strong>OS:</strong>&nbsp;<span>Mac OS X 10.6+</span><br /><strong>Processor:</strong>&nbsp;Dual Core 2Ghz&nbsp;<br /><strong>Memory:</strong>&nbsp;2 GB RAM&nbsp;<br /><strong>Graphics:</strong>&nbsp;1GB &nbsp;DX9 or equivalent OpenGL GPU<br /><br /><span style=\"text-decoration: underline;\"><strong>Linux</strong></span><br /><strong>OS:</strong>&nbsp;<span>Ubuntu 10.10+</span><br /><strong>Processor:</strong>&nbsp;Dual Core 2Ghz&nbsp;<br /><strong>Memory:</strong>&nbsp;2 GB RAM&nbsp;<br /><strong>Graphics:</strong>&nbsp;1GB &nbsp;DX9 or equivalent OpenGL GPU<br /><br /><strong>Recommended:</strong><br /><strong><br /><span style=\"text-decoration: underline;\">Windows</span><br />Processor:</strong>&nbsp;Quad&nbsp;Core<br /><strong>Memory:</strong>&nbsp;4 GB RAM&nbsp;</p>\n<p><span style=\"text-decoration: underline;\"><strong>Mac</strong></span><br /><strong>Processor:</strong>&nbsp;Quad&nbsp;Core<br /><strong>Memory:</strong>&nbsp;4 GB RAM&nbsp;<br /><br /><span style=\"text-decoration: underline;\"><strong>Linux</strong></span><br /><strong>Processor:</strong>&nbsp;Quad&nbsp;Core<br /><strong>Memory:</strong>&nbsp;4 GB RAM&nbsp;</p>",
    "storefront_icon": "/misc/files/hashed/400d0dba5e56d07b6549de3d50c30927f068b158.jpg",
    "pegi_rating": "",
    "storefront_featured_image_large": "/misc/files/hashed/633a04e1f774d2e4faf58e0f49029c2c725c0f1b.jpg",
    "content_types": [
        "game"
    ],
    "storefront_preview_image": null,
    "human_name": "Satellite Reign (Pre-order)",
    "current_price": [
        26.99,
        "USD"
    ],
    "sale_end": 1439398800,
    "sale_type": "normal",
    "full_price": [
        29.99,
        "USD"
    ]

},
    """
    results = []
    is_finish = False
    for i in range(0, 10):
        if is_finish:
            break
        TRACE_INFO['url'] = \
            'https://www.humblebundle.com/store/api/humblebundle?request=2&page_size=20&sort=discount&page=%s' % i
        r = requests.get(TRACE_INFO['url'])
        TRACE_INFO['code'] = r.text
        data = json.loads(r.text)
        for n, sale in enumerate(data['results']):
            TRACE_INFO['code'] = sale

            if not 'sale_type' in sale:
                is_finish = True
                break

            price_old = sale['full_price'][0]
            price_now = sale['current_price'][0]
            discount = 100 - int(price_now / price_old * 100)
            if discount <= 0:
                continue

            results.append({
                'name': sale['human_name'],
                'link': 'https://www.humblebundle.com/store/p/' + sale['machine_name'],
                'image': 'https://www.humblebundle.com' + sale['storefront_featured_image_large'],
                'price_old': price_old,
                'price_now': price_now,
                'discount': discount,
            })

    # if not results:
    #     raise Exception('Sales not found')
    return results