from django.utils.timezone import utc
import re
import requests
import time
import datetime
from parser import parse
from parser.parse import STORES, TRACE_INFO
from parser.models import ParseError, ParseResult
from sales.models import Sale, Store, Chart
import traceback
from django.conf import settings
import os
from PIL import Image
import io


def save_image(url, store_name, sale_id):
    r = requests.get(url, stream=True)
    if not r.status_code == 200:
        return None

    path = settings.MEDIA_ROOT + 'images/' + store_name + '/'
    if not os.path.exists(path):
        os.makedirs(path)

    file_name = str(sale_id) + '.jpg'
    print('Save orig file %s' % file_name)
    with open(path + file_name, 'wb') as f:
        f.write(r.content)

    file_name_h48 = str(sale_id) + '_h48.jpg'
    print('Save file %s' % file_name_h48)
    im = Image.open(io.BytesIO(r.content))
    im.thumbnail((200, 48))
    try:
        im.convert('RGB').save(path + file_name_h48, "JPEG", quality=95)
    except OSError:
        print('Error save to jpeg!')
    return settings.MEDIA_URL + 'images/' + store_name + '/' + file_name


def sales_parse(store_parse=None):
    results = []
    for store_name, params in STORES.items():
        if store_parse and not store_parse == store_name:
            continue

        print('Parse %s' % store_name)
        sales_func = getattr(parse, 'sales_parse_' + store_name, None)
        if not sales_func:
            continue

        sales_len = 0
        try:
            TRACE_INFO.clear()
            TRACE_INFO['store'] = store_name
            sales = sales_func()
        except Exception as error:
            trace = traceback.format_exc()
            ParseError(
                type='sales',
                store=store_name,
                error=error.args[0],
                traceback=trace,
                url=TRACE_INFO['url'],
                code=TRACE_INFO['code'],
            ).save()
            print('Parse error: ' + str(error))
            print(trace)
            print(TRACE_INFO['url'])
            print(TRACE_INFO['code'])
        else:
            sales_len = len(sales)
            sale_active_ids = []
            for parse_sale in sales:
                #print('Save sale: %s' % sale['name'])

                parse_sale['store'] = store_name
                parse_sale['is_active'] = True
                parse_sale['updated'] = datetime.datetime.now().replace(tzinfo=utc)
                parse_sale['price_updated'] = parse_sale['updated']
                parse_sale['expire'] = datetime.datetime.now().replace(tzinfo=utc) + datetime.timedelta(days=1)

                images = parse_sale['image'] if isinstance(parse_sale['image'], list) else [parse_sale['image']]
                parse_sale['image'] = settings.STATIC_URL + 'images/noimage.jpg'

                try:
                    sale = Sale.objects.get(store=store_name, name=parse_sale['name'])
                    if sale.price_now == parse_sale['price_now']:
                        parse_sale['price_updated'] = sale.price_updated
                    for key, value in parse_sale.items():
                        setattr(sale, key, value)
                    sale.save()
                except Sale.DoesNotExist:
                    sale = Sale(**parse_sale)
                    sale.save()

                # Зная идентификатор игры, сохраним картинку с названием, равным идентификатору
                image_url = None
                for image in images:
                    print('Save image %s' % image)
                    image_url = save_image(image, store_name, str(sale.id))
                    time.sleep(1)
                    if image_url:
                        break
                sale.image = image_url if image_url else sale.image
                sale.save()
                sale_active_ids.append(sale.id)

                today = datetime.date.today()
                charts = Chart.objects.filter(sale=sale, created__gte=today, created__lt=today + datetime.timedelta(days=1))
                for chart in charts:
                    chart.delete()

                Chart(
                    sale=sale,
                    price_old=sale.price_old,
                    price_now=sale.price_now,
                    discount=sale.discount,
                ).save()
                time.sleep(1)

            Sale.objects.filter(store=store_name).update(is_active=False)
            Sale.objects.filter(id__in=sale_active_ids).update(is_active=True)
        finally:
            Store.objects.update_or_create(store=store_name, defaults={'count': sales_len})

        results.append({'store': store_name, 'count': sales_len})

    results.sort(key=lambda data: data['store'])
    result = '\n'.join([r['store'] + ':' + str(r['count']) for r in results])
    print('Result: %s' % result)
    ParseResult(result=result).save()


def search_store_parse(store_name, query):
    query = query.lower()
    search_func = getattr(parse, 'search_parse_' + store_name, None)
    if not search_func:
        return []

    games = search_func(query)
    for game in games:
        game['store'] = store_name
    return games


def search_parse(query):
    query = query.lower()
    results = []
    for store_name, params in STORES.items():
        search_func = getattr(parse, 'search_parse_' + store_name, None)
        if not search_func:
            continue

        games = search_func(query)
        for game in games:
            game['store'] = store_name
        results.append(games)
    return results