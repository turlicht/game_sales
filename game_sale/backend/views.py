# -*- coding: utf-8 -*-
from django.views.generic.base import View
from django.http import HttpResponse
from django.core.urlresolvers import get_resolver
import json


class BackendView(View):
    """
    Путь к админке:
    /admin/
    логин:   admin
    пароль:  admin

    Пример успешного ответа, значением result всегда будет массив
    {
       "code": 0,
       "result":
       [
          ...
       ]
    }

    Пример ответа с ошибкой:
    {
       "code": 100,
       "error": "Пользователь уже существует"
    }

    При POST запросах в тело передается JSON с параметрами, например:
    {
        "phone": "79608750025",
        "code": "1234"
    }
    """

    need_auth = False
    need_params = []
    need_files = []

    def __init__(self, **kwargs):
        self.params = {}
        self.user = None
        super(BackendView, self).__init__(**kwargs)

    def dispatch(self, request, *args, **kwargs):
        if self.need_auth and not request.user.is_authenticated():
            return self.response_error(2, 'Необходима авторизация')

        if request.method == 'GET':
            for param in self.need_params:
                value = request.GET.get(param, None)
                if value is None:
                    return self.response_error(3, 'Отсутствует необходимый параметр: %s' % param)

        if request.method in ['POST', 'PUT']:
            if self.need_files:
                for need_file in self.need_files:
                    if not need_file in self.request.FILES:
                        return self.response_error(4, 'Не передан файл: %s' % need_file)

                for param in self.need_params:
                    if not param in self.request.POST:
                        return self.response_error(3, 'Отсутствует необходимый параметр: %s' % param)

            else:
                try:
                    self.params = json.loads(request.body)
                except ValueError:
                    return self.response_error(1, 'Ошибка JSON')

                for param in self.need_params:
                    value = self.params.get(param, None)
                    if value is None:
                        return self.response_error(3, 'Отсутствует необходимый параметр: %s' % param)

        return super(BackendView, self).dispatch(request, *args, **kwargs)

    @staticmethod
    def response(result):
        data = {'result': result, 'code': 0}
        return HttpResponse(json.dumps(data), content_type='application/json')

    @staticmethod
    def response_error(code=1, error='Ошибка запроса'):
        data = {'error': error, 'code': code}
        return HttpResponse(json.dumps(data), content_type='application/json')


class DocsView(View):
    """
    Документация к API
    """
    def get(self, request):
        urls_raw = get_resolver(None).reverse_dict
        urls = []
        for func, url in urls_raw.items():
            if not isinstance(func, str):
                urls.append((url[1], func.__doc__))

        urls = sorted(urls, key=lambda x: x[0])

        result = '<pre>%s</pre><br>' % BackendView.__doc__
        result += '<table border="1">'
        for url, doc in urls:
            result += '<tr><td><pre>%s</pre></td><td><pre>%s</pre></td></tr>' % (url, str(doc).rstrip())
        result += '<table>'

        return HttpResponse(result)