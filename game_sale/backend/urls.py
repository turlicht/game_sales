# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

from backend.views import DocsView


urlpatterns = patterns('',
    url(r'^$', DocsView.as_view()),
)