from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls import handler404
from django.contrib import admin
from django.views.generic import RedirectView
from sales.views import MainView, SaleView

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'game_sale.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^docs/', include('backend.urls')),
    url(r'^sales/', include('sales.urls')),

    url(r'^$', MainView.as_view(), name='main'),
    url(r'^sale/(?P<pk>\d+)/$', SaleView.as_view(), name='sale-detail'),

    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT})
)

handler404 = RedirectView.as_view(permanent=False, url='/')
