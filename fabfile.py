# -*- coding: utf-8 -*-
from fabric.api import run, env, cd, lcd, roles, local, settings
from fabric.decorators import hosts

project_path = '/var/www/game_sales/'


@hosts('turlicht@178.62.163.239')
def apply(commit_text):
    with settings(warn_only=True):
        local('git add --all')
        if commit_text:
            local('git commit -m"%s"' % commit_text)
        else:
            local('git commit')
        local('git push')

    with cd(project_path):
        #run('supervisorctl stop game_sales')
        run('git pull origin master')
        local('touch /tmp/game_sales.touch')
        #run('supervisorctl start game_sales')


def lapply(commit_text=None):
    with settings(warn_only=True):
        local('git add --all')
        if commit_text:
            local('git commit -m"%s"' % commit_text)
        else:
            local('git commit')
        local('git push')

    with lcd(project_path):
        #local('supervisorctl stop game_sales')
        local('git pull origin master')
        local('touch /tmp/game_sales.touch')
        #local('supervisorctl start game_sales')